<?php

namespace Backend\Controllers;

use Backend\Model\Page;
use Helper\Redirect;
use Helper\Session;

/**
 * Управление страницами сайта
 */
class PageController extends AdminController
{
    public function __construct($route)
    {
        $this->layout = 'main';
        $this->route = $route;
        parent::__construct($route);
    }

    /**
     * Показывает список страниц
     */
    public function indexAction()
    {
        $pages = $this->model->findAll();
        $pageExists = Session::get('pageExists');
        Session::set('pageExists', '');
        $this->set(compact('pages', 'pageExists'));
    }

    /**
     * Сохранение отредактированной или вновь созданной страницы
     *
     */
    public function saveAction()
    {        
        $this->view = '';        
        $params = $this->reg->get('req')->post;
        $params['link'] = mb_substr($this->reg->get('str')->translit($params['title']), 0, 160, 'UTF-8');
        $page = $this->model->getPageByAlias($params['link']);
        if ($page && $params['action'] === 'new') {
            Session::set('pageExists', '<p class="text-danger"><b>Не возможно создать. Страница с таким названием уже существует.</b></p>');
        } else {
            $this->model->save($params);
        }
        $cache = $this->reg->get('cache');
        $this->reg->get('cache')->clearCache();
        Redirect::run('page');
    }

    /**
     * Создание новой страницы
     */
    public function newAction()
    {
        $this->view = 'edit';
        $action = 'new';
        $this->reg->get('cache')->clearCache();
        $this->set(compact('action'));
    }

    /**
     * Редактирование страницы
     */
    public function editAction()
    {
        $page = $this->model->edit($this->reg->get('req')->get['id']);
        $this->reg->get('cache')->del($page['link']);
        $this->set(compact('page'));
    }

    public function deleteAction()
    {
       $this->view = '';
       $this->model->del($this->reg->get('req')->get['id']);
       Redirect::run('page');
    }
}
