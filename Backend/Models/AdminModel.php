<?php

namespace Backend\Models;

use Core\Base\Model;

class AdminModel extends Model
{
    public function get()
    {
        return $this->findAll();
    }

    public function getTableContentById($id)
    {
        return $this->findOne($id,'id');
    }

    public function getPageByLink($link)
    {
        return \R::findOne($this->table, '`link`=?', [$link]);
    }

    public function edit($id)
    {        
        return $this->getTableContentById($id);
    }

    public function save($params)
    {
        $tableContent = $this->getTableContentById($params['id']);
        $tableContent['title'] = $params['title'];
        $tableContent['content'] = $params['content'];
        $tableContent['link'] = $params['link'];
        $this->updatePage($tableContent);
    }

    public function getUser($params)
    {
        
        $this->user = $this->findOne($params['name'], "name");
    }

    public function del($id)
    {
        $field = $field ?? $this->primKey;
        $sql = "DELETE FROM {$this->table} WHERE `id` = ? LIMIT 1";
        return $this->pdo->query($sql, [$id]);
    }

    public function updatePage($tableContent)    
    {  
        $params = \Helper\ArrayManipulation::assocToIndex($tableContent);

        $sql = 'UPDATE `page` SET `id` = ?, `title` =  ?, `content` = ? , `author` = ?, `time` = ? ,`edittime` = ?, `link` = ? ,`category` =  ?  WHERE `id` = ' . $tableContent['id'];
        return $this->pdo->query($sql, $params);
    }
}