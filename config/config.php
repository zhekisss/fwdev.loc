<?php
define('WWW', '../Public');
define('CORE', dirname(__DIR__) . '/vendor/framework');
define('ROOT', dirname(__DIR__));
define('CACHE', dirname(__DIR__) . '/tmp/cache');
define('LAYOUT', 'default');
define('DEBUG', true);
define('TIME_ZONE', 'Asia/Yekaterinburg');

//Admin conf
switch ($env) {
  case 'admin':
  define('APP', dirname(__DIR__) . '/Backend')  ;
  define('ENV','admin');
  define('NAME_SPACE','Backend\\');
    break;

//App conf   
  default:
  define('APP', dirname(__DIR__) . '/App');
  define('NAME_SPACE','App\\');
  define('ENV','user');
    break;
}
