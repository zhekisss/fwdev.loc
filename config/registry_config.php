<?php

$config = [
    'components' => [
        'cache' => 'Components\Cache\Cache',
        // 'menu' => 'Vendor\Widgets\Menu\Menu',
        'str' => 'Helper\StringManipulation',
        'req' => 'Helper\Request'
    ]
];

return $config;
