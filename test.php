<?php
/**
 * Класс для измерения времени выполнения скрипта или операций
 */
class Timer
{
    /**
     * @var float время начала выполнения скрипта
     */
    private static $start = .0;

    /**
     * Начало выполнения
     */
    static function start()
    {
        self::$start = microtime(true);
    }

    /**
     * Разница между текущей меткой времени и меткой self::$start
     * @return float
     */
    static function finish()
    {
        return microtime(true) - self::$start;
    }
}

$dsn = 'mysql:dbname=test1;host=127.0.0.1';

$user = 'root';
$password = '';

// try {
//     $db = new PDO ($dsn, $user, $password);
// } catch (Exception $e) {
//   echo "<h1>Подключение не удалось:<br> {$e->getMessage()}</h1>";
//   exit;
// }

$query = "CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(48) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edittime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `link` varchar(255) NOT NULL,
  `category` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$prepare = $db->prepare($query);
// $prepare->bindParam();
$prepare->execute();

echo "<h1>Подключение к базе данных успешно</h>";

$pass = 'admin1@mail.ru123456789';

$hash = password_hash($pass, PASSWORD_BCRYPT);
echo "\n".$hash;

// выводит имя пользователя, от имени которого запущен процесс php/httpd
// (применимо к системам с командой "whoami" в системном пути)
echo exec('whoami');
?>
