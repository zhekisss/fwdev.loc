<?php

namespace Helper;

class Request
{   
    public $get = [];   
    public $post = [];
    public $request = [];   
    public $cookie = [];
    public $files = [];
    public $server = [];
    public $session = [];

    public function __construct()
    {
        $this->get      = $_GET;
        $this->post     = $_POST;
        $this->request  = $_REQUEST;
        $this->cookie   = $_COOKIE;
        $this->files    = $_FILES;
        $this->server   = $_SERVER;
        $this->session  = $_SESSION;
    }
    
    public function filter($var, $type)
    {
        $filter = false;

        switch ($type) {
            case 'email':
                $var = substr($var, 0, 254);
                $filter = FILTER_VALIDATE_EMAIL;
                break;
            case 'int':
                $filter = FILTER_VALIDATE_INT;
                break;
            case 'boolean':
                $filter = FILTER_VALIDATE_BOOLEAN;
                break;
            case 'ip':
                $filter = FILTER_VALIDATE_IP;
                break;
            case 'url':
                $filter = FILTER_VALIDATE_URL;
                break;
        }

        return ($filter === false) ? false : filter_var($var, $filter) !== false ? true : false;
    
    }
}
