<?php

namespace Vendor\Widgets\Menu;

use Core\Base\Model;

class MenuModel extends Model
{
    public $table = 'categories';
}