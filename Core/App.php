<?php

namespace Core;

class App
{
    public static $app;
    public static $di;

    public function __construct()
    {
        $query = strtolower(rtrim($_SERVER['QUERY_STRING'], '/'));
        list($env) = explode('/', $query);
        require_once '../config/config.php';
        self::$di = new DI;
        session_start();
        date_default_timezone_set(TIME_ZONE);
        // new ErrorHandler();
        require_once APP . '/routes.php';
        try {
            Router::dispatch($query, self::$di);
        } catch (Exception $e) {
            echo '<p>' . $e->getMessage() . '</p>';
        }
    }

    protected function getParams()
    {
        $params = require_once CONF . '/params.php';
        if (!empty($params)) {
            foreach ($params as $k => $v) {
                self::$app->setProperty($k, $v);
            }
        }
    }
}
