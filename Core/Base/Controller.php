<?php

namespace Core\Base;

use Core\Registry;

/**
 * Родительский контроллер приложения
 *
 */
abstract class Controller
{
    public $rb;

    /**
     * Путь.
     *
     * @var array
     */
    public $route = [];

    /**
     * Текущий вид
     *
     * @var string
     */
    public $view;

    /**
     * Шаблон.
     *
     * @var string
     */
    public $layout = 'main';

    /**
     * Переменные в вид,
     * пользовательские данные.
     *
     * @var array
     */
    public $vars;

    /**
     * Registry.
     *
     * @var object
     */
    public $reg;

    /**
     * Текущая модель
     */
    public $model;

    public function __construct($route)
    {
        
        
            $classModel = NAME_SPACE . "Models\\" . $route['controller'];            
            $this->model = class_exists($classModel) ? new $classModel : null;
            $this->reg = Registry::getInstance();
            $this->route = $route;
            $this->view = $route['action'];
        
    }

    public function getView()
    {
        $vObj = new View($this->route, $this->layout, $this->view);
        $vObj->render($this->vars);
    }

    public function set($vars)
    {
        $this->vars[] = $vars;
    }

    protected function is_ajax()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $this->layout = 'ajax';
            $this->view = '';

            return true;
        }

        return false;
    }
    
    public function setVar($var, $value)
    {
        $this->vars[$var] = $value;
    }
}
