<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->assets->out(); ?>
    <title><?=$page->title ?? null?></title>
</head>
<body>
    <div class="container">
        <div class="row">
            <header>
                <ul class="menu">
                    <li class="menu-item"><a href="/admin">Admin</a></li>
                    <li class="menu-item"><a href="/">Main</a></li>
                    <li class="menu-item"><a href="/page">Page</a></li>
                    <li class="menu-item"><a href="/posts">Posts</a></li>
                    <li class="menu-item"><a href="/posts-new">Posts-new</a></li>
                    <li class="menu-item"><a href="/contacts">Contacts</a></li>
                </ul>
                <?=$menu ?? null?>
                <h1>MAIN</h1>
                <h3>Header</h3>
            </header>
            <h4>Current view file is "<?=__FILE__?>"</h4>
            
            <div class="content">
                <?=$content;?>                
                 
            </div>
            <footer>
                <h1>FOOTER</h1>
                </footer>
        </div>
        <?php $this->assets->out('js'); ?>
<?=$this->putScripts();?>
</body>
</html>