<?php

namespace App\Controllers;

class PageController extends AppController
{
    public function indexAction()
    {
        $cachePost = $this->reg->get('cache');
        $posts = $cachePost->get('posts');

        if (!$posts ?? false) {
            $posts = $this->model->findAll();
            $cachePost->set('posts', $posts);
        }
        $page = new \StdClass;
        $page->title = 'Страницы';

        $this->set(compact('page', 'posts'));
    }

    public function viewAction()
    {
        $alias = $this->route['alias'];
        $cache = $this->reg->get('cache');
        $page = $cache->get($alias);
        if (!$page ?? false) {
            $page = $this->model->getPageByAlias($alias);
            $cache->set($alias, $page);
        }
        $this->set(compact('page'));
    }
}