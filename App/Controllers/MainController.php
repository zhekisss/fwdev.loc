<?php

namespace App\Controllers;

class MainController extends AppController
{
    public $model;
    protected $cache;
    protected $request;

    public function __construct($route)
    {
        parent::__construct($route);
    }
    
    public function indexAction()
    {        
        $page = (object)['title' => 'MAIN'];
        $this->set(compact('page'));
    }

    /** 
     *
     * @return bool
     */
    public function viewAction()
    {
        $this->view = 'view';
        $alias = $this->route['alias'];
        $this->model->table = 'main_pages';
        if ($page = $this->model->getPageByAlias($alias)) {
            $this->set(compact('page'));
            return true;
        } else {
            return false;
        }
    }
}
