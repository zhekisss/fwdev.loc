<?php

namespace App\Controllers;

use Core\Base\Controller;

/**
 * Общий контроллер для фронтенд части сайта.
 */
class AppController extends Controller
{
    public $reg;

    public $ajax = false;

    public $layout = 'main';

    public $view = 'page';

    public function __construct(array $route)
    {
        if (method_exists($this, $route['action'] . 'Action')) {
            parent::__construct($route);
            $this->getWidgets('menu');
        }
    }

    public function ajax()
    {
        $ajaxArrayConf = [
            'ajax' => 'data',
            'function' => 'test',
        ];
    }   

    public function getWidgets($name)
    {
        $cache = $this->reg->get('cache');
        $widget = $cache->get($name);
        if (!$widget) {
            $widget = $this->reg->get($name);
            $cache->set($name, $widget);
        }
        $this->vars[$name] = $widget;
    }
}
