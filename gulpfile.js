
// =========================================================
// gulpfile.js
// =========================================================
// ------------------------------------------------ requires
import { task, src as _src, dest as _dest, watch, series, parallel } from 'gulp';
import sass, { logError } from 'gulp-sass';
import miniCss from 'gulp-minify-css';
import
// sourceMaps = require('gulp-sourcemaps');
rename from 'gulp-rename';
import plumber from 'gulp-plumber';
import minijs from 'gulp-uglify';

// ------------------------------------------------- configs
var paths = {
    sass: {
        src: './furniture/sass/**/*.{scss,sass}',
        dest: './furniture/css',
        opts: {

        }
    }
};

// ---------------------------------------------- Gulp Tasks
task('sass', function () {
    return _src(paths.sass.src)
        .pipe(sass().on('error', logError))
        .pipe(_dest(paths.sass.dest))
});

// ------------------------------------ Gulp Testing Message
task('message', function () {
    console.log('It works!!');
});

// ---------------------------------------------- Gulp Watch
task('watch:styles', function () {
    watch(paths.sass.src, series('sass'));
});

task('watch', series('sass',
    parallel('watch:styles')
));


// -------------------------------------------- Default task
task('default', series('sass',
    parallel('message', 'watch')
));