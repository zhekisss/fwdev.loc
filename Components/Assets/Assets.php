<?php

namespace Components\Assets;

class Assets
{
    public $path;
    public $assetsJson;

    public function __construct($assetsJson = "assets.json")
    {

        $this->assetsJson = $assetsJson;

        switch (ENV) {
            case "user":
                $this->path = WWW . "/assets/";
                break;
            case "admin":
                $this->path = WWW . "/backendAssets/";
                break;
            default:
                return false;
        }
    }
    
    protected function getAssets(){
        
        return json_decode(file_get_contents($this->path . $this->assetsJson), true);
    }
    
    public function out($type = "css")
    {
        $assets = $this->getAssets($type);
        
        require "template.php";
    }

    public function assetExists($file)
    {
        return file_exists($file) ? false : true ;
    }    
}