<?php

namespace Components\I18n;

interface I18nInterface {

  public function __construct();
  public function getMessage();
  public function setlanguage();
}
